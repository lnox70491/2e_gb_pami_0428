import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CadAutorPage } from './cad-autor.page';

const routes: Routes = [
  {
    path: '',
    component: CadAutorPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CadAutorPageRoutingModule {}