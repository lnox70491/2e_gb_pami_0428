import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CadEditoraPage } from './cad-editora.page';

const routes: Routes = [
  {
    path: '',
    component: CadEditoraPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CadEditoraPageRoutingModule {}