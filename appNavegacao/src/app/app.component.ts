import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  currentPageTitle = 'Dashboard';

  appPages = [
    {
      title: 'Iniciar',
      url: '',
      icon: 'book'
    },
    {
      title: 'Cadastro de Autor',
      url: '/cad-autor',
      icon: 'bookmark'
    },
    {
      title: 'Cadastro de Editoras',
      url: '/cad-editora',
      icon: 'bookmark'
    },
    {
      title: 'Cadastro de Livros',
      url: '/cad-livro',
      icon: 'bookmark'
    },
    {
      title: 'Cadastro de Funcionários',
      url: '/cad-funcionario',
      icon: 'bookmark'
    },
    {
      title: 'Cadastro de Endereços',
      url: '/cad-endereco',
      icon: 'bookmark'
    }
  ];

  constructor() {}
}