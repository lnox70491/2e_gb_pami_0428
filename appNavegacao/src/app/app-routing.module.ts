import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'detalhes',
    loadChildren: () => import('./detalhes/detalhes.module').then( m => m.DetalhesPageModule)
  },
  {
    path: 'cad-editora',
    loadChildren: () => import('./cad-editora/cad-editora.module').then( m => m.CadEditoraPageModule)
  },
  {
    {
      path: 'cad-autor',
      loadChildren: () => import('./cad-autor/cad-autor.module').then( m => m.CadAutorPageModule)
    },
    path: 'cad-livro',
    loadChildren: () => import('./cad-livro/cad-livro.module').then( m => m.CadLivroPageModule),
  {
    path: 'admin-login',
    loadChildren: () => import('./admin/admin-login/admin-login.module').then( m => m.AdminLoginPageModule)
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./admin/dashboard/dashboard.module').then( m => m.DashboardPageModule)
  },
  {
    path: 'cad-funcionarios',
    loadChildren: () => import('./admin/usuarios/cad-funcionarios/cad-funcionarios.module').then( m => m.CadFuncionariosPageModule)
  }
  },
  {
    path: 'cad-usuario',
    loadChildren: () => import('./cad-usuario/cad-usuario.module').then( m => m.CadUsuarioPageModule)
  },
  {
    path: 'cad-usuario',
    loadChildren: () => import('./cad-usuario/cad-usuario.module').then( m => m.CadUsuarioPageModule)
  },
  {
    path: 'cad-endereco',
    loadChildren: () => import('./cad-endereco/cad-endereco.module').then( m => m.CadEnderecoPageModule)
  },
  {
    path: 'cad-funcionario',
    loadChildren: () => import('./cad-funcionario/cad-funcionario.module').then( m => m.CadFuncionarioPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
