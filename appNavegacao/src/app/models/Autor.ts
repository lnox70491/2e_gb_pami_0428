import { Pessoa } from './Pessoa'

export class Autor extends Pessoa {
    nacionalidade: string
    sexo: string
    data_morte: Date
}