import { Pessoa } from './Pessoa'

export class Endereco extends Pessoa {
    id_cliente: number
    descricao: string
    complemento: string
}