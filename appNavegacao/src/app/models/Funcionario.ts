import { Pessoa } from './Pessoa'

export class Funcionario extends Pessoa {
    data_admissao: Date
    salario: number
}

enum Cargo {
    atendente = 0,
    repositor = 1,
    logistica = 2,
    gestor = 3
}