import { Usuario } from './../../models/Usuario';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.page.html',
  styleUrls: ['./admin-login.page.scss'],
})
export class AdminLoginPage implements OnInit {
  private user: Usuario

  constructor(
    private usuariosService: UsuariosService
  ) { 
    this.user = new Usuario()
  }

  ngOnInit() {
  }

  logar (): void {
    console.log(this.user)
    this.usuariosService.logar(this.user)
    .subscribe({
      next: (dados) => {
        console.log(dados)
      },
      error: (erro) => {
        console.error(erro)
      }
    })
  }
}
