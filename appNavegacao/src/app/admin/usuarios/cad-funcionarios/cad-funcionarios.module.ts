import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CadFuncionariosPageRoutingModule } from './cad-funcionarios-routing.module';

import { CadFuncionariosPage } from './cad-funcionarios.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CadFuncionariosPageRoutingModule
  ],
  declarations: [CadFuncionariosPage]
})
export class CadFuncionariosPageModule {}
