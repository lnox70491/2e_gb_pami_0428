import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AutoresService {
  private readonly URL_Y = "https://8100-lnox70491-2egbpami0428-td4hp4qh8bh.ws-us78.gitpod.io"
  private readonly URL_N = ""
  private readonly URL = this.URL_Y

  constructor(
    private http: HttpClient
  ) { }

  salvar(autor): Observable<any> {
    return this.http.post<any>(`${this.URL}autor`, autor)
  }
}