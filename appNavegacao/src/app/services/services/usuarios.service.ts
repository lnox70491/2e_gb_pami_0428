import { Usuario } from './../../models/Usuario';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {
  private readonly URL_Y = "https://8100-lnox70491-2egbpami0428-td4hp4qh8bh.ws-us78.gitpod.io"
  private readonly URL_N = ""
  private readonly URL = this.URL_Y

  constructor(
    private http: HttpClient
  ) { }

  logar(usuario: Usuario): Observable <any> {
    return this.http.post<any>(`${this.URL}usuario/login`, usuario)
  }
}